package com.zeenux.quizapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.zeenux.quizapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
   // private lateinit var trueButton: Button
   // private lateinit var falseButton: Button
    private val questionBank= listOf(
        Question(R.string.question_australia,true),
        Question(R.string.question_oceans,true),
        Question(R.string.question_mideast,false),
        Question(R.string.question_africa,false),
        Question(R.string.question_americas,true),
        Question(R.string.question_asia,true)

    )
    private var currentIndex=0
    private lateinit var binding:ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        //setTitle(R.string.app_name)
        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
       // trueButton=findViewById(R.id.true_button)
       // falseButton=findViewById(R.id.false_button)

        binding.trueButton.setOnClickListener { view: View ->
// Do something in response to the click here
            //Toast.makeText(this,R.string.correct_toast,Toast.LENGTH_SHORT).show()
        checkAnswer(true)
        }

        binding.falseButton.setOnClickListener { view: View ->

// Do something in response to the click here
            //Toast.makeText(this,R.string.incorrect_toast,Toast.LENGTH_SHORT).show()
            //com.google.android.material.snackbar.Snackbar snackbar=Snackbar.make(coordinatorLayout,"This is a simple Snackbar",Snackbar.LENGTH_SHORT).show()
        //Snackbar.make(view,R.string.incorrect_toast,Snackbar.LENGTH_SHORT).show()
            checkAnswer(false)
        }
        binding.nextButton.setOnClickListener{view: View ->
            currentIndex=(currentIndex+1)%questionBank.size
            updateQuestion()
        }
//        val questionTextResId=questionBank[currentIndex].textResId
//        binding.questionTextView.setText(questionTextResId)
        updateQuestion()
    }

    private fun updateQuestion(){
        val questionTextResId=questionBank[currentIndex].textResId
        binding.questionTextView.setText(questionTextResId)
    }

    private fun checkAnswer(userAnswer: Boolean){
        val correctAnswer=questionBank[currentIndex].answer
        val messageResId=if(userAnswer==correctAnswer){
            R.string.correct_toast
        }else {
            R.string.incorrect_toast
        }
        Toast.makeText(this,messageResId,Toast.LENGTH_SHORT).show()
    }
}